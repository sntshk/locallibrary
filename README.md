# Local Library

Local Library demonstrates a book management system made in Django.

See https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django

## TODO

- [ ] Implement _Create_, _Update_, and _Delete_ views for BookInstance, Genre and Languages.